import math
import random
import copy
from renpy import config
from renpy.display.dragdrop import Drag
from renpy.store import Image
#from renpy.exports import call_screen
from renpy.game import call_in_new_context
import time


def drag_token(drags, drop):
    if isinstance(drop, DropZone):
        (x, y) = drop.board_position
        board = drags[0].board
        if(board.place_token(drags[0], x, y)):
            board.pass_turn()

class Vector2():

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distance(self, pos):
        return math.sqrt((pos.x - self.x) * (pos.x - self.x) + (pos.y - self.y) * (pos.y - self.y))


class DropZone(Drag):
    image = "images/teeko/drop_zone.png"

    def __init__(self, x, y, *args, **kwargs):
        self.board_position = (x, y)
        kwargs['drag_name'] = "(%d, %d)" % (x, y)
        kwargs['draggable'] = False
        kwargs['xcenter'] = config.screen_height / 3 + 238 + 81 * y
        kwargs['ycenter'] = config.screen_width / 6 - 18 + 84 * x
        super(DropZone, self).__init__(*args, **kwargs)
        self.set_child(Image(self.image))


class Token(Drag):

    def __init__(self, team, board=None, number=0, *args, **kwargs):
        self.team = team
        self.board = board
        if team == 0:
            self.image = "images/teeko/black_token.png"
            self.name = "black_token_"
            kwargs['draggable'] = True
            kwargs['xcenter'] = config.screen_width / 8
        else:
            self.image = "images/teeko/red_token.png"
            self.name = "red_token_"
            kwargs['draggable'] = False
            kwargs['xcenter'] = config.screen_width / 8 * 7
        kwargs['ycenter'] = number * 100 + config.screen_height / 3
        self.name += str(number)
        kwargs['drag_name'] = self.name
        kwargs['dragged'] = drag_token
        super(Token, self).__init__(*args, **kwargs)
        self.set_child(Image(self.image))

    def __str__(self):
        return "Token\n" + "team : " + str(self.team) + "\n"


class Board(object):

    image = "images/teeko/teeko_board.jpg"

    def __init__(self, label_win, label_loose, *args, **kwargs):
        super(Board, self).__init__(*args, **kwargs)
        self.label_win = label_win
        self.label_loose = label_loose
        self.clear()

    def _generate_tokens_for_team(self, team_number):
        for i in range(4):
            token = Token(team_number, board=self, number=i)
            self.unplayed_tokens.append(token)

    def clear(self):
        self.round_nb = 0
        self.player_turn = 0
        self.phase = 1
        self.unplayed_tokens = []
        self.played_tokens = []
        self.children = []
        self._generate_tokens_for_team(0)
        self._generate_tokens_for_team(1)
        self.grid = [
            [None, None, None, None, None],
            [None, None, None, None, None],
            [None, None, None, None, None],
            [None, None, None, None, None],
            [None, None, None, None, None]
        ]
        self.drop_zones = []
        for i in range(0, 5):
            for j in range(0, 5):
                self.drop_zones.append(DropZone(i, j))

    def pass_turn(self):
        if self.player_turn == 0:
            self.player_turn = 1
            self.ia_play()
        else:
            self.player_turn = 0
            self.round_nb += 1
        if self.round_nb >= 4 and self.phase == 1: 
            self.phase = 2
            self.round_nb += 1
        
        if self.is_victory(1):
            call_in_new_context(self.label_win)
        elif self.is_victory(2):
            call_in_new_context(self.label_loose)

    def is_victory(self, team):
        simple_grid = SimpleGrid(self)
        if simple_grid.is_win(team):
            return True
        return False
        

    def get_drop_zone(self, coord):
        for drop_zone in self.drop_zones:
            if drop_zone.board_position == coord:
                return drop_zone
        return None
            

    def generate_next_situations_tree(self, grid, team, depth):
        if depth == 0:
            return AlphaBetaNode(grid, team)
        else:
            node = AlphaBetaNode(grid, team)
            next_situations = grid.generate_next_situations(team)
            for situation in next_situations:
                node.children.append(self.generate_next_situations_tree(situation, (team%2)+1, depth - 1))
            return node

    def min_max(self, node, team, first_iter):
        if len(node.children) == 0:
            return [node.grid, node.grid.evaluate(2)]
        else:
            values = []
            grids = []
            for child in node.children:
                child_result = self.min_max(child, (team % 2) + 1, False)
                grids.append(child_result[0])
                values.append(child_result[1])
            if(first_iter):
                max_value = max(values)
                if values.count(max(values)) > 1:
                    near_values = []
                    for grid in grids:
                        near_values.append(grid.evaluate(2))
                    max_near_value = max(near_values)
                    return [grids[near_values.index(max_near_value)], max_near_value]
                return [grids[values.index(max_value)], max_value]
            else:
                if team == 1:
                    return [node.grid, max(values)]
                else:
                    return [node.grid, min(values)]


    def alpha_beta(self, node, team, alpha, beta, first_iter):
        if len(node.children) == 0:
            return [node.grid, node.grid.evaluate(2)]
        else:
            values = []
            grids = []
            if team == 2:
                v = float("inf")
                for child in node.children:
                    child_result = self.alpha_beta(child, (team % 2) + 1, alpha, beta, False)
                    values.append(child_result[1])
                    v = min(v, child_result[1])
                    if alpha > v:
                        return [node.grid, v]
                    beta = min(beta, v)
            else:
                v = float("-inf")
                for child in node.children:
                    child_result = self.alpha_beta(child, (team % 2) + 1, alpha, beta, False)
                    grids.append(child_result[0])
                    values.append(child_result[1])
                    v = max(v, child_result[1])
                    if v > beta:
                        if first_iter:
                            if values.count(max(values)) > 1:
                                near_values = []
                                for grid in grids:
                                    near_values.append(grid.evaluate(2))
                                max_near_value = max(near_values)
                                return [grids[near_values.index(max_near_value)], max_near_value]
                            return [grids[values.index(max(values))], v]
                        return [node.grid, v]
                    alpha = max(alpha, v)

            if first_iter:
                if values.count(max(values)) > 1:
                    near_values = []
                    for grid in grids:
                        near_values.append(grid.evaluate(2))
                    max_near_value = max(near_values)
                    return [grids[near_values.index(max_near_value)], max_near_value]
                return [grids[values.index(max(values))], v]
            else:
                return [node.grid, v]

    def ia_play(self):

        tree_depth = 3
        if self.round_nb >= 1:
            tree_depth = 4
        if self.round_nb >= 3:
            tree_depth = 5

        simple_grid = SimpleGrid(self)
        tree = self.generate_next_situations_tree(simple_grid, 2, tree_depth)
        best_situation = self.alpha_beta(tree, 1, float("-inf"), float("inf"), True)[0]
                    
        token = None
        x = 0
        y = 0

        if self.phase == 1:
            token = [token for token in self.unplayed_tokens if token.team == 1][0]

            
        for i in range(0, 5):
            for j in range(0, 5):
                if simple_grid.grid[i][j] != best_situation.grid[i][j] and best_situation.grid[i][j] == 2:
                    x = i
                    y = j

                if simple_grid.grid[i][j] != best_situation.grid[i][j] and best_situation.grid[i][j] == 0:
                    if self.phase == 2:
                        print("DEBUG : IA TOKEN PLACE IN : (" + str(i) + "," + str(j) + ")")
                        token = self.grid[i][j]

        self.place_token(token, x, y)
        self.pass_turn()

    def is_move_possible(self, token, x, y):
        (old_x, old_y) = token.pos
        return ((x == old_x + 1 or x == old_x - 1 or x == old_x) and
                (y == old_y + 1 or y == old_y - 1 or y == old_y) and
                not (x == old_x and y == old_y) and
                x >= 0 and y >= 0 and x <= 4 and y <= 4 and
                self.grid[x][y] == None)


    def place_token(self, token, x, y):

        drop_zone = [zone for zone in self.drop_zones if zone.board_position == (x,y)][0]
        if(self.player_turn == 0): #player playing
            if self.phase == 1: #1st phase of the game
                if token in self.unplayed_tokens:
                    if self.grid[x][y] == None:
                        self.grid[x][y] = token
                        token.snap(drop_zone.x, drop_zone.y)
                        self.unplayed_tokens.remove(token)
                        self.played_tokens.append(token)
                        token.pos = (x, y)
                    else:
                        return False
                else:
                    print("Player attempted a forbidden move")
                    drop_zone = self.get_drop_zone(token.pos)
                    token.snap(drop_zone.x,drop_zone.y)
                    return False
            else: #2nd phase of the game
                if self.is_move_possible(token, x, y):
                    (old_x, old_y) = token.pos
                    self.grid[x][y] = token
                    token.snap(drop_zone.x, drop_zone.y)
                    self.grid[old_x][old_y] = None
                    token.pos = (x, y)
                else:
                    print("Player attempted a forbidden move")
                    drop_zone = self.get_drop_zone(token.pos)
                    token.snap(drop_zone.x,drop_zone.y)
                    return False
        else: #IA Playing
            if self.phase == 1: #1st phase of the game
                self.grid[x][y] = token
                token.snap(drop_zone.x, drop_zone.y)
                self.unplayed_tokens.remove(token)
                self.played_tokens.append(token)
                token.pos = (x, y)
            else: #2nd phase of the game
                (old_x, old_y) = token.pos
                self.grid[x][y] = token
                token.snap(drop_zone.x, drop_zone.y)
                self.grid[old_x][old_y] = None
                token.pos = (x, y)
        return True

class AlphaBetaNode():

    def __init__(self, grid, team):
        self.children = []
        self.grid = grid
        self.team = team

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__) + "\n" + str(self.children)

class SimpleGrid():

    def __init__(self, grid=None, *args, **kwargs):
        self.grid = []
        if grid is not None:
            if isinstance(grid, Board):
                for line in grid.grid:
                    generated_line = []
                    for token in line:
                        generated_line.append((0 if token is None else token.team + 1))
                    self.grid.append(generated_line)

            elif isinstance(grid, SimpleGrid):
                for line in grid.grid:
                    generated_line = []
                    for team in line:
                        generated_line.append(team)
                    self.grid.append(generated_line)

        else:
            self.grid = [
                [0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0]
            ]

    def __str__(self):
        return str(self.grid)

    def generate_poss(self, coord):
        poss_list = []
        i = coord[0]
        j = coord[1]

        # square possibilities
        if self.is_inside(i - 1, j) and self.is_inside(i, j - 1):
            poss_list.append([(i - 1, j), (i - 1, j - 1), (i, j - 1)])
        if self.is_inside(i, j - 1) and self.is_inside(i + 1, j):
            poss_list.append([(i, j - 1), (i + 1, j - 1), (i + 1, j)])
        if self.is_inside(i, j + 1) and self.is_inside(i + 1, j):
            poss_list.append([(i, j + 1), (i + 1, j + 1), (i + 1, j)])
        if self.is_inside(i - 1, j) and self.is_inside(i, j + 1):
            poss_list.append([(i - 1, j), (i - 1, j + 1), (i, j + 1)])

        # line possibilities
        if self.is_inside(i + 3, j):
            poss_list.append([(i + 1, j), (i + 2, j), (i + 3, j)])
        if self.is_inside(i - 1, j) and self.is_inside(i + 2, j):
            poss_list.append([(i - 1, j), (i + 1, j), (i + 2, j)])
        if self.is_inside(i - 2, j) and self.is_inside(i + 1, j):
            poss_list.append([(i - 2, j), (i - 1, j), (i + 1, j)])
        if self.is_inside(i - 3, j):
            poss_list.append([(i - 3, j), (i - 2, j), (i - 1, j)])

        if self.is_inside(i, j + 3):
            poss_list.append([(i, j + 1), (i, j + 2), (i, j + 3)])
        if self.is_inside(i, j - 1) and self.is_inside(i, j + 2):
            poss_list.append([(i, j - 1), (i, j + 1), (i, j + 2)])
        if self.is_inside(i, j - 2) and self.is_inside(i, j + 1):
            poss_list.append([(i, j - 2), (i, j - 1), (i, j + 1)])
        if self.is_inside(i, j - 3):
            poss_list.append([(i, j - 3), (i, j - 2), (i, j - 1)])

        # diag possibilities
        if self.is_inside(i + 3, j + 3):
            poss_list.append([(i + 1, j + 1), (i + 2, j + 2), (i + 3, j + 3)])
        if self.is_inside(i + 2, j + 2) and self.is_inside(i - 1, j - 1):
            poss_list.append([(i - 1, j - 1), (i + 1, j + 1), (i + 2, j + 2)])
        if self.is_inside(i - 2, j - 2) and self.is_inside(i + 1, j + 1):
            poss_list.append([(i - 2, j - 2), (i - 1, j - 1), (i + 1, j + 1)])
        if self.is_inside(i - 3, j - 3):
            poss_list.append([(i - 3, j - 3), (i - 2, j - 2), (i - 1, j - 1)])

        if self.is_inside(i - 3, j + 3):
            poss_list.append([(i - 1, j + 1), (i - 2, j + 2), (i - 3, j + 3)])
        if self.is_inside(i - 2, j + 2) and self.is_inside(i + 1, j - 1):
            poss_list.append([(i + 1, j - 1), (i - 1, j + 1), (i - 2, j + 2)])
        if self.is_inside(i + 2, j - 2) and self.is_inside(i - 1, j + 1):
            poss_list.append([(i + 2, j - 2), (i + 1, j - 1), (i - 1, j + 1)])
        if self.is_inside(i + 3, j - 3):
            poss_list.append([(i + 3, j - 3), (i + 2, j - 2), (i + 1, j - 1)])

        return poss_list

    def is_ennemy(self, i, j, allied_team):
        return (self.grid[i][j] != 0 and self.grid[i][j] != allied_team)

    def is_inside(self, i, j):
        return (i >= 0 and i <= 4 and j >= 0 and j <= 4)

    def randomize(self):
        for k in range(0, 5):
            for l in range(0, 5):
                self.grid[k][l] = 0
        for k in range(0, 4):
            self.grid[random.randint(0, 4)][random.randint(0, 4)] = 1
        for k in range(0, 4):
            self.grid[random.randint(0, 4)][random.randint(0, 4)] = 2

    def generate_next_situations(self, team_playing, state=3):
        """
        function generating every possible next state from a given situation
        @team_playing : the player who has to play ( 1 or 2 )
        @state : the state of the game : 1 for the first 4 plays, 2 for the rest of the game, 3 for auto behaviour.
        auto behaviour : the function will guess the state of the game, for a minor cost in performance. Auto behaviour is enabled by default.
        @returns the list of every possible stae. If the win is already secured, no move can be made : hence, the function returns the actual grid
        """
        if self.is_win(team_playing) or self.is_win((team_playing % 2) + 1):
            return [self]
        
        if state == 3:
            playing_token = 0
            for i in range(0, 5):
                for j in range(0, 5):
                    playing_token += 1 if self.grid[i][j] == team_playing else 0
            state = 1 if playing_token < 4 else 2

        situation_list = []
        if state == 1:
            for i in range(0, 5):
                for j in range(0, 5):
                    if self.grid[i][j] == 0:
                        grid2 = copy.deepcopy(self)
                        grid2.grid[i][j] = team_playing
                        situation_list.append(grid2)

        elif state == 2: #todo : bug here
            for i in range(0, 5):
                for j in range(0, 5):
                    if self.grid[i][j] == team_playing:
                        for k in [-1, 1]:
                            for l in [-1, 1]:
                                if self.is_inside(i + k, j + l) and self.grid[i + k][j + l] == 0:
                                    grid2 = copy.deepcopy(self)
                                    grid2.grid[i + k][j + l] = team_playing
                                    grid2.grid[i][j] = 0
                                    situation_list.append(grid2)
        return situation_list

    def get_team_tokens(self, team):
        allied_tokens = []
        for i in range(0,5):
            for j in range(0,5):
                if self.grid[i][j] == team:
                    allied_tokens.append((i,j))
        return allied_tokens
        

    def is_win(self, team):
        allied_tokens = self.get_team_tokens(team)
        for allied_token in allied_tokens:
            poss_list = self.generate_poss(allied_token)
            for poss in poss_list:
                if self.rank_poss(poss, team) == 4:
                    return True
        return False
                
    def rank_poss(self, poss, team):
        """
        @returns the rank of a possibility ( a possibility being a possible winning combination )
        0 : an ennemy is blocking the possibility
        1-4 : number of present tokens on the possibility
        """
        rank = 1
        for coord in poss:
            if self.grid[coord[0]][coord[1]] == team:
                rank += 1
            elif self.is_ennemy(coord[0], coord[1], team):
                rank = 0
                break;
        return rank
                
            
            
    def evaluate(self, team):
        score = 0
        ennemy_tokens = self.get_team_tokens((team % 2) + 1)
        allied_tokens = self.get_team_tokens(team)

        for ennemy_token in ennemy_tokens:
            poss_list = self.generate_poss(ennemy_token)
            for poss in poss_list:
                if self.rank_poss(poss, (team % 2) + 1) == 4:
                    return 0               

        for allied_token in allied_tokens:
            poss_list = self.generate_poss(allied_token)
            for poss in poss_list:
                if self.rank_poss(poss, team) == 4:
                    return float("inf")
                poss_rank = self.rank_poss(poss, team)
                score += 10 ** ((2 * poss_rank) - 2)

                
        return score
