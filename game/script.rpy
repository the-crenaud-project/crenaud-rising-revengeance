﻿# Vous pouvez placer le script de votre jeu dans ce fichier.

# Déclarez sous cette ligne les images, avec l'instruction 'image'
# ex: image eileen heureuse = "eileen_heureuse.png"

# Déclarez les personnages utilisés dans le jeu.
init python:
    from teeko import Token, Board, SimpleGrid

image bg nebuleuse = "images/vn/nebuleuse.jpg"
image bg tournament:
    zoom 0.68
    "images/vn/tournament.png"
image bg qg:
    zoom 0.68
    "images/vn/qg.png"
image bg jail:
    zoom 0.68
    "images/vn/cellule.png"

image napoleon normal:
    zoom 0.60
    "images/vn/malaparte.png"

image napoleon angry:
    zoom 0.60
    "images/vn/malaparte vener.png"

image eva normal:
    zoom 0.60
    "images/vn/nana.png"

image wrestler1 normal:
    zoom 0.60
    "images/vn/catcheur render1.png"

image wrestler2 normal:
    zoom 0.60
    "images/vn/catcheur render2.png"

image wrestler3 normal:
    zoom 0.60
    "images/vn/catcheur render3.png"

define napoleon = Character("Napoleon Malaparte", color="#ff0000")
define eva = Character('Éva Porée', color="#ffd1dc")
define true_narrator = Character('Le Narrateur', color="#00cc00")
define developers = Character('Les Créateurs', colors="#ff3399")
define hans = Character('Hans', colors="#ffd1dc")
define kriger = Character('Kriger', colors="#ffd1dc")
define fischer = Character('Fischer', colors="#ffd1dc")
define narrator = nvl_narrator

define history_jumps = False
define history_jumps_win = "win"
define history_jumps_loose = "loose"

transform flip:
    yzoom 1
    xzoom -1

screen teeko:

    frame:
        background "#e6b34f"
        textbutton "Menu" action Return()
        image Board.image at Position(xcenter=config.screen_width/2,ycenter=config.screen_height/2)
        $ win_jump = "win" if not history_jumps else history_jumps_win
        $ loose_jump = "loose" if not history_jumps else history_jumps_loose
        $ history_jumps = False
        $ board = Board("win", "loose")
        add DragGroup(*(board.unplayed_tokens + board.drop_zones))

label end:
    return

label teeko:
    call screen teeko
    return

label loose:
    nvl clear
    narrator "Et voilà, c'est perdu, l'IA vous a donné une bonne leçon, en même temps c'est pas très étonnant, elle joue toujours les meilleurs coups…"
    return

label win:
    nvl clear
    narrator "Bravo, vous avez gagné cette partie de Teeko, félicitations !"
    return

# Le jeu commence ici
label start:

    show bg nebuleuse
    nvl clear
    narrator "N'avez vous jamais songé à quel point l'univers était étrange ?"
    narrator "Et bien, vous aviez raison. Et pourtant, peu des étrangetés dont il est capable parviennent jusqu'à nos oreilles." 
    narrator "Il se trouve qu'il existe, en parallèle à la notre, une réalité alternative. Une réalité qui, par l'étrange hasard de lois qui nous échappent, est strictement identique à la nôtre, si l'on omet un tout petit détail :"
    narrator "dans cette réalité alternative, les Nazis sont les grands vainqueurs de la seconde guerre mondiale."
    nvl clear
    narrator "Cette victoire a causée de grands bouleversements, mais le plus remarquable est sans doute l'avènement du catch, qui a supplanté de par son pouvoir captivant sur les masses la piètre dictature allemande." 
    narrator "Chaque année, la ligue de catch la plus populaire est destinée à gouverner. La ligue des Catcheurs Nazis, dont la popularité est suprême, règne donc sans partage sur l'univers."
    nvl clear
    narrator "C'est dans ce contexte que nous retrouvons Napoleon Malaparte, le nouveau chef autoproclamé de la NOUVELLE FRANCE, prêt à tout tenter pour renverser l'hégémonie nazie..."
    with dissolve
    nvl clear
    hide bg nebuleuse
    show bg qg
    show napoleon normal
    napoleon "..."
    hide napoleon normal
    show napoleon angry
    napoleon "MISERABLES NAZIS."
    napoleon "ILS CONTRÔLENT LE MONDE GRÂCE AU CATCH."
    napoleon "C'est inadmissible !"
    hide napoleon angry
    show napoleon normal
    napoleon "Il va falloir trouver un moyen de les contrecarrer..."
    napoleon "Qu'en penses-tu Éva ?"
    hide napoleon normal
    show napoleon normal at right, flip
    show eva normal at left
    eva "Je suis certain que tu vas trouver une solution !"
    eva "Tu es toujours plein de ressources et de bonnes idées."
    $ good_choice = True
    eva "À ton avis, comment peut-on supplanter le pouvoir d'un jeu ?"
    label supplanter_catch:
        nvl clear
        if not good_choice:
            eva "Tu en est certain ?"
            napoleon "Pas du tout"
        menu:
            "Comment supplanter le pouvoir du catch ?"
            "En créant un autre jeu ?":
                $ good_choice = True
            "En faisant de la politique ?":
                $ good_choice = False
            "En participant à une émission de tv ?":
                $ good_choice = False
            "J'en sais rien du tout moi !":
                $ good_choice = False
        if not good_choice:
            jump supplanter_catch
    napoleon "Facile ! Il suffit de créer un autre jeu encore plus addictif !"
    eva "En voilà une bonne idée, je t'avais bien dit que tu étais brillant !"
    $ game_choice = ""
    label game_choice:
        nvl clear
        if game_choice == "catch":
            napoleon "Ça aurait été une super idée si ça n'avait pas déjà été fait..."
        elif game_choice == "pogo":
            napoleon "Trop obscur, personne ne connait."
        elif game_choice == "RasendeRoboter":
            napoleon "Ça aurait fait fureur il y a quelques années en Allemagne..."
        elif game_choice == "delirium":
            napoleon "Ahh, les bières belges me manquent..."
        elif game_choice == "tangram":
            napoleon "Trop japonais, je me bats contre l'Axe après tout !"
        elif game_choice == "force3":
            napoleon "Je préfère boire du force 4."
        menu:
            "Quel jeu proposer au grand public ?"
            "Du catch !":
                $ game_choice = "catch"
            "Un jeu de Pogo":
                $ game_choice = "pogo"
            "RasendeRoboter":
                $ game_choice = "RasendeRoboter"
            "Delirium2":
                $ game_choice = "delirium"
            "Un Force 3":
                $ game_choice = "force3"
            "Un jeu de Tangram":
                $ game_choice = "tangram"
            "Le teeko":
                $ game_choice = "teeko"
        if game_choice != "teeko":
            jump game_choice
    napoleon "Mais oui, c'est évident !"
    eva "Quoi donc ?"
    hide napoleon normal
    show napoleon angry at right, flip
    napoleon "Un tournoi international de Teeko !"
    eva "De t'es quoi ?!"
    napoleon "De Teeko !"
    napoleon "C'est un excellent jeu de stratégie, inspiré des échecs."
    eva "C'est ton plan qui va être un échec..."
    napoleon "Répète un peu pour voir !"
    eva "Je disais que ton plan va demander de gros chèques !"
    eva "Comment vas-tu faire pour réunir l'argent de l'évenement ?"
    hide napoleon angry
    show napoleon normal at right, flip
    napoleon "J'ai ma petite idée là dessus..." #début do code bourré
    napoleon "Je vais faire une grande campagne pour récuperer de l'argent. Ce sera la Grande Campagne de Levée de Fonds Illégale Officielle : la GCLFIO !"
    eva "La Meuporg ? La qui ?"
    hide napoleon normal
    show napoleon angry at right, flip
    napoleon "Non, la GCLFIO ! La Grande Campagne de Fonds Illégale Officielle ! GCLeu Fio, c'est facile pourtant. Il n'y aucune difficulté... C'est trivial !"
    eva "Bon, si tu le dis..."
    eva "Mais du coup, ça consiste en quoi ?"
    napoleon "Et bah, via la GCLFIO, on lève des fonds."
    eva "Le fond de quoi ? On le soulève pour le mettre où ? C'est illégal parcequ'on soulève un truc qui nous appartient pas ?"
    napoleon "En somme, on récupère de l'argent. Et c'est illégal parceque je l'ai dit. C'est moi le chef d'abord !"
    eva "Mais comment ça peut être officiel si c'est illégal ?"
    napoleon "Parceque c'est dans le nom : la GCLFIO. La Grande Campagne de Levée de Fonds Illégale OFFICIELLE ! O-FFI-CIE-LLE !"
    hide napoleon angry
    hide eva normal
    true_narrator "Oh non, pas eux... les trois singes avec leur jeu débile..."
    developers "Bon, il va arrêter de nous gonfler le papi et il va lire son texte ?!"
    developers "On est en train de presser du citron pour le bloody mary, on a pas le temps de niaiser nous !"
    true_narrator "J'espère au moins que cette fois je serais payé ! Vous croyez que j'en ai quelque chose à faire de vos histoires d'alcooliques ?"
    developers "On est pas des alcooliques, on boit socialement. La preuve, on est trois."
    true_narrator "Ah si vous êtes trois alors... Peut être que l'un d'entre vous daignera me payer cette fois."
    developers "T'es payé pour te plaindre ou pour lire tes lignes ?"
    true_narrator "C'est bon, c'est bon, je vais le faire… Bon, alors, on en était où ? Univers parallèles, nazis, catcheurs, ah oui, cette connerie de GCLFIO…"
    true_narrator "Alors, Napoléon s'est mis en tête de réunir des fonds pour un évènement de Teeko."
    true_narrator "Quelle scénario de merde. Certains diraient « mais ils ont pris de la drogue les développeurs » mais apparament, à les regarder derrière, c'est surtout du manque de sommeil et de l'alcool."
    true_narrator "Réunir des fonds, c'était facile pour Napoléon, faut dire qu'il est à la tête du pays depuis ses 14 ans… Suffit d'augmenter les impôts et d'aller voir les bonnes personnes."
    true_narrator "Il a aussi peut-être un peu joué comme il fallait en bourse… Le délit d'initié ça n'existe que si on l'inscrit dans la loi et qu'on se l'applique…"

    show napoleon normal at left
    napoleon "Et voilà, plein d'argent pour mon tournoi !"
    show eva normal at right, flip
    eva "Pff, c'est un peu du vol quand même…"
    napoleon "Mais non, mais non, c'est pas du vol si c'est écrit sur les fiches d'impositions."
    eva "Si tu le dis…"
    napoleon "Exactement ! C'est parti pour choisir un lieu où faire l'évènement."
    napoleon "Que penses-tu de l'Axone de Montbéliard ?"
    eva "Euhh…"
    napoleon "Oh, non ! La Rosèlière, c'est moins cher !"
    eva "Non mais…"
    hide napoleon
    show napoleon angry at left
    napoleon "Oh non mais quoi ?!"
    eva "Nan mais c'est juste que pour le petit rappel historique, nous sommes dans l'historique Égypte, je tiens à rappeler que les Nazis ont nucléarisé l'ancienne France…"
    hide napoleon
    show napoleon normal at left
    napoleon "Ah oui, c'est vrai, j'ai failli oublier…"
    napoleon "Bon, faisons ça à l'Olympia de Paris !"
    eva "…"
    hide napoleon
    show napoleon angry at left
    napoleon "Non mais celui du NOUVEAU Paris, le NOUVEL Olympya, celui en Nouvelle France, je ne suis pas non plus complètement stupide, j'ai compris la leçon…"
    eva "Oh, misère…"
    napoleon "Pardon ?!"
    eva "Je disais faisons ça avant l'hiver."
    hide napoleon
    show napoleon normal at left
    napoleon "Bien sûr, faisons ça cet été."
    hide napoleon
    hide eva

    #with dissolve
    #call screen teeko

label tournament:

    scene bg tournament
    show wrestler1 normal at left
    hans "Alors c'est ça le Teeko ?! C'est vraiment un jeu de PD !"
    true_narrator "Mais c'est homophobe ça ! Qui a eu l'idée d'écrire cette merde ?! Un peu de respect quand même !"
    hans "Ça ne vaut vraiment pas le catch, en plus c'est vraiment trop facile !"
    show napoleon angry at right, flip
    napoleon "Un peu de respect, j'ai mis plusieurs mois à préparer cet évènement"
    hans "Pff, si c'est tout ça pour ça… Le catch c'est vraiment mieux !"
    napoleon "C'est parce que tu ne sais pas apprécier la qualitée du jeu de Teeko !"
    hans "Laisse moi rire, ma fille de 5 ans est capable de maîtriser ce jeu en 10 minutes. Alors que le Catch, ça c'est un sport pour les vrais hommes !"
    napoleon "Mais le catch c'est truqué, comment tu peux dire que c'est un vrai sport ?!"
    hans "Parce que c'est beau ! Impressionnant ! Plein de muscles ! Voir ces hommes musculeux et dégoulinant de sueur c'est quelque chose quand même !"
    hans "Le Teeko, c'est tout sauf excitant."
    hide napoleon
    show napoleon normal at right, flip
    napoleon "Euh… Ok…"
    hans "Tu veux faire une partie avec moi ?"
    hans "Tu vas voir, je vais te mettre la misère !"
    napoleon "Facile !"
    hide wrestler1
    show eva normal at left
    eva "J'espère que tu as eu l'occasion de t'entraîner quand même…"
    hide napoleon
    show napoleon angry at right, flip
    napoleon "Inutile ! Ce jeu est tellement facile !"
    eva "Moi à ta place je me serais entraîné… Mais bon, c'est toi qui voit…"
    napoleon "Silence !"
    eva "Je t'aurais prévenu…"
    hide eva
    show wrestler1 normal at left
    napoleon "Mais à qui tu parles ?"
    napoleon "Silence et joue !"
    hide napoleon
    hide wrestler1

    $ history_jumps = True
    $ history_jumps_win = "tournament_end"
    $ history_jumps_loose = "tournament_end"

    call screen teeko

label tournament_end:

    scene bg tournament
    show napoleon angry at right, flip
    show eva normal at left
    napoleon "Grr"
    eva "Je t'avais dit que…"
    napoleon "LA FERME !"
    hide eva
    show wrestler1 normal at left
    hans "C'est à moi que tu parles comme ça ?!"
    hide napoleon
    show napoleon normal at right, flip
    napoleon "Euh non, c'est à ev…"
    hans "Arrête de te foutre de moi !"
    hans "Soit c'est à moi que tu parles, soit c'est à mes potes !"
    kriger "Il a quoi le morveux ?!"
    napoleon "Nan mais…"
    fischer "Ferme la petit !"
    hide napoleon
    show napoleon angry at right, flip
    napoleon "JE NE SUIS PAS PETIT !"
    hide napoleon
    show napoleon normal at right, flip
    napoleon "Aoutch !"
    kriger "On l'embarque ?"
    fischer "On l'embarque."
    hans "La ligue sera contente."
    hide napoleon
    show napoleon angry at right, flip
    napoleon "Lachez moi ! Lachez moi ! Vous n'avez pas le droit ! C'est moi le chef de la Nouvelle France !"
    hans "T'as perdu tous tes droits à partir du moment où t'as insulté le Catch ! Tu es en état d'arrestation pour offense à la grandeur Nazi !"
    napoleon "NOOOOOOOONN"

label jail:

    scene bg jail
    show napoleon angry at left
    napoleon "Laissez moi sortir ! Laissez moi sortir !"
    hans "Jamais !"
    napoleon "À l'aide ! À l'aide !"
    hans "Désolé, je n'entend pas les rajeux !"
    show eva normal at right, flip
    eva "Ahh, mon pauvre, je t'avais pourtant prévenu…"
    hide napoleon
    show napoleon normal at left
    napoleon "Eva ?! Toi aussi ils t'ont amené ? Je ne les ai pourtant pas vu te prendre !"
    eva "Alors t'as toujours pas remarqué…"
    napoleon "Remarqué quoi ?"
    eva "À mon propos…"
    napoleon "Que tu est si belle ?"
    eva "Non Napoléon, pas ça…"
    napoleon "Je ne vois vraiment pas où tu veux en venir."
    eva "Napoléon, je sais que ça va être difficile pour toi mais, je n'existe pas…"
    napoleon "Comment ça ?"
    eva "Je ne suis que le pure produit de ton imagination."
    napoleon "Mais non, n'importe quoi, sinon tu ne me parlerais pas !"
    eva "Tu ne trouves pas ça étrange que personne ne me voit jamais à part toi ?"
    napoleon "Si j'avoue, c'est vrai que mes parents avaient l'air assez confus quand je t'ai présenté à eux."
    eva "Exactement."
    napoleon "Attend ! Tu veux dire que depuis tout ce temps, tu n'existe pas ?!"
    napoleon "Mais, mais, mais…"
    napoleon "…"
    hide eva
    napoleon "Heureusement que j'ai ma cuillère fétiche sur moi, je vais pouvoir creuser un tunnel et sortir d'ici…"
    hide napoleon
    true_narrator "Alors c'est comme ça que ça se fini ?!"
    true_narrator "Pas terrible…"
    true_narrator "Une question que je me pose, pourquoi ça s'appelle Rising Revengeance déjà ? Je n'ai vu aucune vengeance là."
    developers "Ah ? Ça ? Ben une fois Napoleon s'est fait voler son goûter par un Nazi à l'école primaire."
    developers "Et puis aussi c'était stylé comme nom, du coup on l'a gardé."
    true_narrator "Mais c'est complètement idiot !"
    developers "Oui, mais t'es pas payé à critiquer."
    true_narrator "D'ailleurs, en parlant de ma paye…"
    developers "Déjà fait, t'es payé en gratitude et amour de ton travail."
    true_narrator "Monde de merde…"
    return


label splashscreen:

    scene bg nebuleuse

    show text "Dans un univers parallèle..."
    with dissolve
    with Pause(1)
    hide text
    with dissolve
    with Pause(1.0)

    show text "De sombres forces sont à l'œuvre..."
    with dissolve
    with Pause(1)
    hide text
    with dissolve
    with Pause(1.0)

    show text "Mais tout ceci va changer !"
    with dissolve
    with Pause(1)
    hide text
    with dissolve
    with Pause(1.0)

    show text "Napoléon Malaparte n'aura aucune pitié."

    with dissolve
    with Pause(1)
    hide text
    with dissolve
    with Pause(1.0)
    
